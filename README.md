# AdGuardHome Upstream
[![upstream](https://img.shields.io/badge/LICENSE-BSD3%20Clause%20Liscense-brightgreen?style=flat-square)](https://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_(%22BSD_License_2.0%22,_%22Revised_BSD_License%22,_%22New_BSD_License%22,_or_%22Modified_BSD_License%22))

[README](README.md) | [中文文檔](README_zh.md)

## Edit

First of all, you must change the setting to use upstream file.<br>
You can find the option `upstream_dns_file` in `/usr/local/AdGuardHome/AdGuardHome.yaml` and it should be `/usr/local/AdGuardHome/upstream.txt`.<br>
For more information, please read [the official AdGuardHome documentation](https://github.com/AdguardTeam/AdGuardHome/wiki/Configuration).

## Get

Obtain scripts through this project and grant script execution permissions.

```
curl -o /usr/local/bin/upstream.sh https://gitlab.com/fernvenue/upstream/-/raw/master/upstream.sh
chmod +x /usr/local/bin/upstream.sh
```

## Run

```
/bin/bash /usr/local/bin/upstream.sh
```

## Auto

```
curl -o /lib/systemd/system/upstream.service https://gitlab.com/fernvenue/upstream/-/raw/master/upstream.service
curl -o /lib/systemd/system/upstream.timer https://gitlab.com/fernvenue/upstream/-/raw/master/upstream.timer
systemctl enable upstream.timer
systemctl start upstream.timer
systemctl status upstream
```

## Q&A

Q: How can I switch to the IPv6 version?<br>
A: Just change the url link in shell.<br>

Q: Why change to daily update?<br>
A: This change was made after noticing the update frequency of the chinalist project, and it has been confirmed that the influence of daily updates is very small.
