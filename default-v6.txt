#default
tls://101.6.6.6:8853
https://101.6.6.6:8443/dns-query

#google
tls://[2001:4860:4860::8888]
tls://[2001:4860:4860::8844]
tls://8.8.8.8
tls://8.8.4.4

#cloudflare
tls://[2606:4700:4700::1001]
tls://[2606:4700:4700::1111]
tls://1.0.0.1
tls://1.1.1.1
https://1.0.0.1/dns-query
https://1.1.1.1/dns-query

#chinalist
