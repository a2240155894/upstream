# AdGuardHome Upstream
[![upstream](https://img.shields.io/badge/LICENSE-BSD3%20Clause%20Liscense-brightgreen?style=flat-square)](https://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_(%22BSD_License_2.0%22,_%22Revised_BSD_License%22,_%22New_BSD_License%22,_or_%22Modified_BSD_License%22))

[README](README.md) | [中文文檔](README_zh.md)

## 編輯

在開始之前，你必須變更 AdGuardHome 上游設置為文件方式。<br>
你可以在 `/usr/local/AdGuardHome/AdGuardHome.yaml` 中找到 `upstream_dns_file` 選項並修改為 `/usr/local/AdGuardHome/upstream.txt` 來完成變更。<br>
獲取更多相關訊息可以透過 [AdGuardHome 官方文檔](https://github.com/AdguardTeam/AdGuardHome/wiki/Configuration)。

## 獲取

你需要通過本項目獲取脚本並授予執行權限。

```
curl -o /usr/local/bin/upstream.sh https://gitlab.com/fernvenue/upstream/-/raw/master/upstream.sh
chmod +x /usr/local/bin/upstream.sh
```

## 執行

```
/bin/bash /usr/local/bin/upstream.sh
```

## 自動化

```
curl -o /lib/systemd/system/upstream.service https://gitlab.com/fernvenue/upstream/-/raw/master/upstream.service
curl -o /lib/systemd/system/upstream.timer https://gitlab.com/fernvenue/upstream/-/raw/master/upstream.timer
systemctl enable upstream.timer
systemctl start upstream.timer
systemctl status upstream
```

## 問答

問：我該如何切換到 IPv6 版本？<br>
答：你只需要在脚本中替換超鏈接。<br>

問：為什麽轉變為每日更新？<br>
答：這項更改是在註意到chinalist項目的更新頻率之後進行的，並且已經確認每日更新的影響很小。
